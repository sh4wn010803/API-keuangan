const express = require("express");
const router = express.Router();

const{
    postTranskas,
    getId,
    putTranskas
} = require("../controllers/Supplier");

// Endpoints
router.post("/",postTranskas);
router.get("/:id",getId)
router.put("/",putTranskas)

module.exports = router;
