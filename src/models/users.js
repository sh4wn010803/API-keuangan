const sequelize = require("../database/conn")
const { Model, DataTypes } = require("sequelize");

const Users = sequelize.define('Users', {
  id_user:{
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  username: {
    type: DataTypes.STRING,
  },
  idbas: {
    type: DataTypes.STRING,
  },
},
{
  sequelize,
  modelName: "Users",
  tableName: "users",
  timestamps: false,
})

module.exports = Users;